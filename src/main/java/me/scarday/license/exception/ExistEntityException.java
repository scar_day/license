package me.scarday.license.exception;

public class ExistEntityException extends RuntimeException{

    public ExistEntityException(String message) {
        super(message);
    }
}
