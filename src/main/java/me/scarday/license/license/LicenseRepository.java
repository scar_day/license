package me.scarday.license.license;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LicenseRepository extends JpaRepository<LicenseEntity, Integer> {

    LicenseEntity findByKey(String key);

    void deleteByKey(String key);
}
