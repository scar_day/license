package me.scarday.license.license;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import me.scarday.license.exception.ExistEntityException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class LicenseService {
    LicenseRepository licenseRepository;

    public LicenseEntity createLicense(String key, String productName) {
        if (licenseRepository.findByKey(key) != null) {
            throw new ExistEntityException("License already exist");
        }

        LicenseEntity license = new LicenseEntity(null, key, productName);

        return licenseRepository.save(license);
    }

    public void deleteLicense(String key) {
        licenseRepository.deleteByKey(key);
    }
}
