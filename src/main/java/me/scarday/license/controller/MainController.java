package me.scarday.license.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import me.scarday.license.exception.ExistEntityException;
import me.scarday.license.exception.MissingException;
import me.scarday.license.exception.NotFoundException;
import me.scarday.license.license.LicenseEntity;
import me.scarday.license.license.LicenseRepository;
import me.scarday.license.license.LicenseService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MainController {
    LicenseRepository licenseRepository;
    LicenseService licenseService;

    @GetMapping("/get.license/{key}")
    public ResponseEntity<LicenseEntity> infoLicense(@PathVariable(name = "key") String license) {
        if (license == null) {
            throw new MissingException("Missing parameter: key");
        }

        LicenseEntity entity = licenseRepository.findByKey(license);

        if (entity == null) {
            throw new NotFoundException("License not found");
        }

        return ResponseEntity.ok(entity);
    }

    @PutMapping(value = "/create.license")
    public ResponseEntity<LicenseEntity> createLicense(
            @RequestBody(required = false) LicenseEntity entity
    ) {
        if (entity.getProduct() == null) {
            throw new MissingException("Missing parameter: product_name");
        }

        if (entity.getKey() == null) {
            throw new MissingException("Missing parameter: license_key");
        }

        LicenseEntity createdEntity = licenseService.createLicense(entity.getKey(), entity.getProduct());

        return ResponseEntity.ok(createdEntity);
    }

}
